# Release Checklist

- check build via `cargo make icons-build-with-registry-mi`
- check test via `cargo make icons-ff-test`
- fix all warnings too
- fill CHANGELOG.md in gen and icons
- set planned versions in Cargo.toml
- adjust icons/README.md versions and doc in general
- adjust versions and documentation in seed_icons_browser
- publish gen crate with `cargo make gen-publish`
- tag with `git tag gen-<version>`
- check with `cargo make dry-icons-publish`
- publish icons crate with `cargo make icons-publish`
- tag with `git tag icons-<version>`
- publish seed_icons_browser site
