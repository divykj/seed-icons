# Icons Collections for Seed

[Seed](https://seed-rs.org/) is an awesome framework to develop
single page applications.

This is a utility crate containing functions to use during 
build of other packages, for example
[seed-icons](https://crates.io/crates/seed-icons).

## get_fa_resources 

To use [Font-Awesome](https://fontawesome.com/) collection of icons easier
this crate provides `get_fa_resources` function, which you can use
to download folder `font-awesome` to specified location. 
Inside `font-awesome` you'd find stylesheets required to link from your
`index.html`.
The idea is to call that function from your `build.rs` and ignore 
`font-awesome` folder from VCS, so that icons resources would 
be downloaded during build of your SPA.

Read more in [seed-icons](https://crates.io/crates/seed-icons).

## get_mi_resources

This method allows to download resources to use material icons,
which is a part of [material design](http://material.io/). 

Unfortunately this way of downloading resources only supports
`regular` icons. Other types of icons, which are also part 
of this collection (`outlined`, `round`, `sharp`, etc) are not
included and therefore it's recommened to get necessary resources
manually from [Google Fonts](https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp) instead.

## write_all

Crate provides `write_all` function to generate icons to use with Seed.
`write_all` is intended to be used from `seed-icons` package, during
its build and you probably don't need to use it by yourself.

`write_all` does not provide you with icons themselves (you stil 
have to get and reference CSS / fonts files from your index.html), 
but it generates `icons.rs` file with module for each icon, which 
should give you a convenient way to use them in your application.

## registry

Registry is a several generated methods, such as `font_awesome_collection`
and `material_icons_collection`. Those methods return a generic structures
with information about available collections, which later are used in 
`seed_icons_browser` crate to dynamicly display all available icons.