// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use serde::Deserialize;
use serde_json::json;
extern crate attohttpc;
use crate::{IconsCollectionSource, errors::GenIconsErr};
use crate::IconSource;
use codegen::Module;
use codegen::Scope;
use inflector::Inflector;
use std::fs::File;
use std::io::prelude::*;

pub static FA_RELEASE: &str = "5.14.0";

#[derive(Debug, Deserialize)]
struct FaResponse {
    data: FaData,
}

#[derive(Debug, Deserialize)]
struct FaData {
    release: FaRelease,
}

#[derive(Debug, Deserialize)]
struct FaRelease {
    icons: Vec<FaIcon>,
}

#[derive(Debug, Deserialize)]
struct FaIcon {
    id: String,
    membership: FaMembership,
}

#[derive(Debug, Deserialize)]
struct FaMembership {
    free: Vec<String>,
}

#[derive(Clone, Debug)]
pub enum IconType {
    SOLID,
    REGULAR,
    BRANDS,
}

#[derive(Debug, Clone)]
pub struct FaIconSource {
    pub name: String,
    pub icon_type: IconType,
}

impl crate::IconSource for FaIconSource {
    fn closure_view_call(&self) -> String {
        let type_module_name = self.icon_type.to_string().to_lowercase();
        format!(
            "crate::fa::{}::{}::i()",
            type_module_name,
            crate::module_name(&self.name)
        )
    }

    fn closure_view_with_classes_call(&self) -> String {
        let type_module_name = self.icon_type.to_string().to_lowercase();
        format!(
            "crate::fa::{}::{}::i_c(classes)",
            type_module_name,
            crate::module_name(&self.name),
        )
    }

    fn view_call_example(&self) -> String {
        let type_module_name = self.icon_type.to_string().to_lowercase();
        format!(
            "seed_icons::fa::{}::{}::i()",
            type_module_name,
            crate::module_name(&self.name)
        )
    }

    fn name(&self) -> String {
        self.name.clone()
    }
}

pub fn write_font_awesome_icons(
    scope: &mut Scope,
    enable_styles: bool,
    maybe_filter: Option<Vec<String>>,
) -> Result<(), GenIconsErr> {
    let module = scope.new_module("fa").vis("pub");
    // add_imports(module, enable_styles);
    let icons = get_icons(FA_RELEASE)?;
    icons.into_iter().for_each(|icon| {
        if maybe_filter
            .clone()
            .map(|filter| !filter.contains(&icon.name()))
            .or_else(|| Some(false))
            .unwrap()
        {
            return;
        }
        let type_module_name = icon.icon_type.to_string().to_lowercase();
        let type_module = module.get_module_mut(&type_module_name);
        let type_module_ref: &mut codegen::Module = if type_module.is_none() {
            module.new_module(&type_module_name).vis("pub")
        } else {
            type_module.unwrap()
        };

        add_fa_module(icon.name, icon.icon_type, type_module_ref, enable_styles);
    });
    Ok(())
}

fn add_fa_module(name: String, icon_type: IconType, fa_module: &mut Module, enable_styles: bool) {
    let type_class = match icon_type {
        IconType::SOLID => "fas",
        IconType::REGULAR => "far",
        IconType::BRANDS => "fab",
    };

    let kebab_case_name: String = if name != "500px" {
        name.to_kebab_case()
    } else {
        String::from("500-px")
    };

    let module_name = crate::module_name(&name);
    if fa_module.get_module(&module_name).is_some() {
        return;
    }
    let module = fa_module.new_module(&module_name).vis("pub");

    crate::add_imports(module, enable_styles);
    let classes = format!(r#""{}", "fa-{}""#, type_class, kebab_case_name);
    crate::add_functions(module, classes, enable_styles, None);
}

fn icons(body: FaResponse) -> Vec<FaIconSource> {
    body.data
        .release
        .icons
        .into_iter()
        .flat_map(|fa_icon| {
            let id = fa_icon.id;
            fa_icon
                .membership
                .free
                .into_iter()
                .filter_map(|style| match &style[..] {
                    "solid" => Some(IconType::SOLID),
                    "regular" => Some(IconType::REGULAR),
                    "brands" => Some(IconType::BRANDS),
                    _ => None,
                })
                .map(move |icon_type| FaIconSource {
                    name: id.clone(),
                    icon_type: icon_type,
                })
        })
        .collect()
}

fn file_name(release: &str) -> String {
    format!("font-awesome-{}.json", release)
}

fn drop_to_file(ok_res: attohttpc::Result<Vec<u8>>, release: &str) {
    let mut file = File::create(file_name(release)).unwrap();
    match ok_res {
        Ok(bts) => {
            let c: &[u8] = &bts; // c: &[u8]
            file.write_all(c).unwrap();
        }
        _ => (),
    }
}

fn result(ok_res: attohttpc::Response) -> Result<Vec<FaIconSource>, GenIconsErr> {
    match ok_res.json::<FaResponse>() {
        Err(e) => Err(GenIconsErr::HttpError(e)),
        Ok(body) => Ok(icons(body)),
    }
}

fn get_query(release: &str) -> serde_json::value::Value {
    json!({
        "query": format!(
            r#"query {{
                release(version: "{}") {{ 
                    icons {{ 
                        id, membership {{ free }} 
                    }} 
                }} 
            }}"#, release),
    })
}

fn req_and_drop(release: &str) -> Result<(), GenIconsErr> {
    let graphql_query = get_query(release);
    let url = "https://api.fontawesome.com";
    let resp = attohttpc::post(url).json(&graphql_query)?.send()?;
    match resp.error_for_status() {
        Ok(ok_res) => Ok(drop_to_file(ok_res.bytes(), release)),
        Err(e) => Err(GenIconsErr::HttpError(e)),
    }
}

pub fn get_icons(release: &str) -> Result<Vec<FaIconSource>, GenIconsErr> {
    // req_and_drop(release)?;
    let mut file = match File::open(file_name(release)) {
        Ok(file) => file,
        Err(_) => {
            req_and_drop(release)?;
            File::open(file_name(release))?
        }
    };
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let resp: FaResponse = serde_json::from_str(&*contents)?;
    Ok(icons(resp))
}

pub fn req_icons(release: &str) -> Result<Vec<FaIconSource>, GenIconsErr> {
    // req_and_drop(release)?;
    let graphql_query = get_query(release);
    let url = "https://api.fontawesome.com";
    let resp = attohttpc::post(url).json(&graphql_query)?.send()?;
    match resp.error_for_status() {
        Ok(ok_res) => result(ok_res),
        Err(e) => Err(GenIconsErr::HttpError(e)),
    }
}

pub fn registry_icons(
    collections: &mut Vec<IconsCollectionSource>,
    maybe_filter: &Option<Vec<String>>,
) -> Result<(), GenIconsErr> {
    let fa_icons = get_icons(FA_RELEASE)?;
    let fa_icons: Vec<Box<dyn IconSource>> = fa_icons
        .into_iter()
        .map(|it| {
            let boxed: Box<dyn IconSource> = Box::new(it);
            boxed
        })
        .filter(|icon| {
            maybe_filter
                .clone()
                .map(|filter| filter.contains(&icon.name()))
                .or_else(|| Some(true))
                .unwrap()
        })
        .collect();
    let fa_icons = IconsCollectionSource {
        icons: fa_icons,
        function_prefix: "font_awesome".to_string(),
    };
    collections.push(fa_icons);

    Ok(())
}
