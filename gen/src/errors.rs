// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use std::io;

#[derive(Debug)]
pub enum GenIconsErr {
    HttpError(attohttpc::Error),
    SerdeError(serde_json::Error),
    IOError(io::Error),
}

impl std::convert::From<attohttpc::Error> for GenIconsErr {
    fn from(error: attohttpc::Error) -> Self {
        GenIconsErr::HttpError(error)
    }
}

impl std::convert::From<serde_json::Error> for GenIconsErr {
    fn from(error: serde_json::Error) -> Self {
        GenIconsErr::SerdeError(error)
    }
}

impl std::convert::From<io::Error> for GenIconsErr {
    fn from(error: io::Error) -> Self {
        GenIconsErr::IOError(error)
    }
}
