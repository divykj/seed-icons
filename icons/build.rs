// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use seed_icons_gen;
use std::env;

fn main() -> Result<(), seed_icons_gen::GenIconsErr> {
    let path = env::var("OUT_DIR").unwrap();
    seed_icons_gen::write_all(
        &path,
        if cfg!(feature = "styles") {
            true
        } else {
            false
        },
        if cfg!(feature = "font_awesome") {
            true
        } else {
            false
        },
        if cfg!(feature = "material_icons") {
            true
        } else {
            false
        },
    )
}
