# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.0] - 2020-10-18
### Added
- material icons
- registry (intended to be used from seed_icons_browser only), enabled by feature `registry`
- features for `font_awesome` and `material_icons` collections
- default features is `["font_awesome"]`
- changelog
- Makefile.toml, using `cargo make` now
- env variable `SEED_ICONS_FILTER_NAMES` support

### Changed
- newer version of font-awesome: 5.14.0
- optimized font-awesome generation