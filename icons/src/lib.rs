// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

include!(concat!(env!("OUT_DIR"), "/fa/icons.rs"));

#[cfg(feature = "registry")]
include!(concat!(env!("OUT_DIR"), "/gen_registry.rs"));

#[cfg(feature = "registry")]
mod registry;

#[cfg(feature = "registry")]
pub use registry::registry::*;

#[cfg(feature = "registry")]
pub fn collections<T>() -> Vec<IconCollection<'static, T>> {
    vec![
        #[cfg(feature = "font_awesome")]
        IconCollection{
            name: "Font-Awesome".to_string(),
            icons: font_awesome_collection()
        }, 
        #[cfg(feature = "material_icons")]
        IconCollection{
            name: "Material Icons".to_string(),
            icons: material_icons_collection()
        }
    ]
}
