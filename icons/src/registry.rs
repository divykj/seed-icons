// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

#[cfg(feature = "registry")]
pub mod registry {
    use seed::{prelude::*};

    pub struct RegisteredIcon<'a, T> {
        pub name: String,
        pub view: &'a dyn Fn() -> Node<T>,
        pub view_with_classes: &'a dyn Fn(Vec<&str>) -> Node<T>,
    }
    pub struct IconCollection<'a, T> {
        pub name: String,
        pub icons: Vec<RegisteredIcon<'a, T>>,
    }
}
